using LoginExperiments.Database;
using LoginExperiments.Extensions;
using LoginExperiments.Login;
using LoginExperiments.Services;
using LoginExperiments.Utils;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using System;

namespace LoginExperiments
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

			#region Database

			services.AddDbContext<MyContext>(options =>
			{
				options.UseInMemoryDatabase("LoginExperiments");
			});

			#endregion Database

			#region Configuration

			services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
			services.AddResponseCompression();

			#endregion Configuration

			#region Cookies

			services.Configure<AntiforgeryOptions>(options =>
			{
				options.Cookie.Name = "LoginExperiments.Security";
				options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
				options.Cookie.SameSite = SameSiteMode.Strict;

				options.FormFieldName = "__SecurityToken";
			});

			#endregion Cookies

			#region Services and DI

			services.AddSingleton(Configuration);

			services.AddSingletonHostedService<InitService>();

			#endregion Services and DI

			#region Authentication

			services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
				.AddCookie(options =>
				{
					options.Cookie.Name = "LoginExperiments.Auth";
					options.Cookie.SecurePolicy = isDev ? CookieSecurePolicy.SameAsRequest : CookieSecurePolicy.Always;
					options.Cookie.SameSite = SameSiteMode.Strict;

					options.LoginPath = "/account/login";
					options.LogoutPath = "/account/logout";
					options.AccessDeniedPath = "/error/403";
					options.EventsType = typeof(CustomCookieAuthEvents);
					options.SlidingExpiration = true;
				});
			services.AddScoped<CustomCookieAuthEvents>();

			services.AddFido2(options =>
			{
				string url = Environment.GetEnvironmentVariable("ASPNETCORE_APPL_URL") ?? "http://localhost";
				string path = Environment.GetEnvironmentVariable("ASPNETCORE_APPL_PATH") ?? "";
				var uri = new Uri(url + path);

				options.ServerDomain = uri.Host;
				options.ServerName = "Login Experiments";

				options.Origin = url;
				options.TimestampDriftTolerance = (int)TimeSpan.FromSeconds(5).TotalMilliseconds;
			});

			#endregion Authentication

			#region MVC

			services.AddControllersWithViews()
				.AddNewtonsoftJson(options =>
				{
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				});

			#endregion MVC
		}

		public void Configure(IApplicationBuilder app, IServiceScopeFactory serviceScopeFactory)
		{
			bool isDev = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development";

			app.UseProxyHosting();
			app.UseResponseCompression();

			if (isDev)
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/error");
			}

			app.Use(async (httpContext, next) =>
			{
				// Add request path for error page
				string path = httpContext.Request.Path.ToString();
				if (!path.StartsWith("/error", StringComparison.OrdinalIgnoreCase))
					httpContext.Items[HttpItem.OriginalPath] = path;

				await next();
			});
			app.UseStatusCodePagesWithReExecute("/error/{0}");

			app.UseStaticFiles();
			app.UseAuthentication();

			app.UseRewriter(new RewriteOptions()
				.AddMfa(serviceScopeFactory)
				.AddUserChecks(serviceScopeFactory));

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllerRoute(
					name: "default",
					pattern: "{controller=Home}/{action=Index}/{id?}");
			});
		}
	}
}
