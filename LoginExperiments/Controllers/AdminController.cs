﻿using Fido2NetLib.Objects;
using LoginExperiments.Database;
using LoginExperiments.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace LoginExperiments.Controllers
{
	[Authorize]
	public class AdminController : Controller
	{
		private readonly ILogger logger;
		private readonly MyContext dbContext;

		public AdminController(ILogger<AdminController> logger, MyContext dbContext)
		{
			this.logger = logger;
			this.dbContext = dbContext;
		}

		public IActionResult Index()
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();
			if (!authUser.IsAdmin)
				return Forbid();

			var users = dbContext.Users
				.Include(u => u.WebAuthnKeys)
				.OrderBy(u => u.Username)
				.ToList();

			ViewData["Title"] = "Userlist";
			return View(users);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult SwitchPassword(int id)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();
			if (!authUser.IsAdmin)
				return Forbid();

			var user = dbContext.Users
				.Where(u => u.Id == id)
				.FirstOrDefault();
			if (user == null)
				return NotFound();

			user.RequireNewPassword = !user.RequireNewPassword;
			dbContext.SaveChanges();

			return Json(user.RequireNewPassword ? "YES" : "NO");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Delete(int id)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();
			if (!authUser.IsAdmin)
				return Forbid();

			if (authUser.Id == id)
				return BadRequest();

			var user = dbContext.Users
				.Include(u => u.WebAuthnKeys)
				.Where(u => u.Id == id)
				.FirstOrDefault();
			if (user == null)
				return NotFound();

			dbContext.WebAuthnKeys.RemoveRange(user.WebAuthnKeys);
			dbContext.Users.Remove(user);
			dbContext.SaveChanges();

			return Ok();
		}
	}
}