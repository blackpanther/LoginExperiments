﻿using LoginExperiments.Models;
using LoginExperiments.Utils;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace LoginExperiments.Controllers
{
	[Route("Error")]
	public class ErrorController : Controller
	{
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Index()
		{
			return Index(500);
		}

		[Route("{id}")]
		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Index(int id)
		{
			ViewData["Title"] = $"Error {id}";

			string requestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
			string originalPath = HttpContext.Items[HttpItem.OriginalPath]?.ToString();
			if (!string.IsNullOrWhiteSpace(originalPath))
				originalPath = $" (<code>{originalPath}</code>)";

			var model = new ErrorViewModel
			{
				ErrorCode = id,
				Title = $"Error {id}",
				Description = $"An error occurred. If this happens frequently, please contact an administrator. Request-ID: <code>{requestId}</code>."
			};

			switch (id)
			{
				case 400:
					model.Title = "Bad Request";
					model.Description = "You sent a invalid or incomplete request that could not be processed.";
					break;

				case 401:
					model.Title = "Unauthorized";
					model.Description = "There was an error evaluating whether you're allowed to access the document. Please sign in.";
					break;

				case 403:
					model.Title = "Forbidden";
					model.Description = "You do not have the permission to access the requested document.";
					break;

				case 404:
					model.Title = "Not Found";
					model.Description = $"The requested document{originalPath} was not found.";
					break;

				case 500:
					model.Title = "Internal Server Error";
					model.Description = $"The server encountered an unexpected condition or configuration. Request-ID: <code>{requestId}</code>.";
					break;
			}

			return View(model);
		}
	}
}