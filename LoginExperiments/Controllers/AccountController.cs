﻿using Fido2NetLib;
using Fido2NetLib.Objects;
using LoginExperiments.Database;
using LoginExperiments.Database.Entities;
using LoginExperiments.Extensions;
using LoginExperiments.Login;
using LoginExperiments.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OtpNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginExperiments.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private readonly ILogger logger;
		private readonly MyContext dbContext;
		private readonly IFido2 fido;

		private static readonly Dictionary<string, AssertionOptions> assertOptions = new Dictionary<string, AssertionOptions>();
		private static readonly Dictionary<string, (CredentialCreateOptions Options, bool IsLogin)> createOptions = new Dictionary<string, (CredentialCreateOptions, bool)>();

		public AccountController(ILogger<AccountController> logger, MyContext dbContext, IFido2 fido)
		{
			this.logger = logger;
			this.dbContext = dbContext;
			this.fido = fido;
		}

		#region User profile

		public IActionResult Index()
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			var user = dbContext.Users
				.Include(u => u.WebAuthnKeys)
				.Where(u => u.Id == authUser.Id)
				.FirstOrDefault();
			if (user == null)
				return NotFound();

			ViewData["Title"] = "Profile";
			return View(user);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult Index(User model)
		{
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			ViewData["Title"] = "Profile";

			authUser.Firstname = model.Firstname?.Trim();
			authUser.Lastname = model.Lastname?.Trim();

			if (!string.IsNullOrWhiteSpace(model.NewPassword))
			{
				if (string.IsNullOrWhiteSpace(model.CurrentPassword))
					ModelState.AddModelError(nameof(model.CurrentPassword), "The current password is required");
				if (string.IsNullOrWhiteSpace(model.PasswordRepeat))
					ModelState.AddModelError(nameof(model.PasswordRepeat), "The new password has to be repeated");

				if (!ModelState.IsValid)
					return View(model);

				if (model.NewPassword.Trim() != model.PasswordRepeat.Trim())
					ModelState.AddModelError(nameof(model.PasswordRepeat), "The passwords do not match");
				if (!SecurityHelper.VerifyPassword(model.CurrentPassword, authUser.PasswordHash, out _))
					ModelState.AddModelError(nameof(model.CurrentPassword), "The password is invalid");

				if (!ModelState.IsValid)
					return View(model);

				authUser.PasswordHash = SecurityHelper.HashPassword(model.NewPassword.Trim());
			}

			dbContext.SaveChanges();
			return RedirectToAction("Index");
		}

		#region WebAuthn

		public IActionResult GetRegisterOptions(bool isLogin = false)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			var fidoUser = new Fido2User
			{
				Id = SafeInt(authUser.Id),
				Name = authUser.Username,
				DisplayName = authUser.GetName()
			};

			var existingCredentials = dbContext.WebAuthnKeys
				.Where(k => k.UserId == authUser.Id)
				.ToList();
			var keys = existingCredentials
				.Select(c => JsonConvert.DeserializeObject<PublicKeyCredentialDescriptor>(c.DescriptorJson))
				.ToList();

			var selection = new AuthenticatorSelection
			{
				RequireResidentKey = isLogin,
				UserVerification = isLogin ? UserVerificationRequirement.Preferred : UserVerificationRequirement.Discouraged
			};

			var extensions = new AuthenticationExtensionsClientInputs
			{
				Extensions = true,
				Location = true,
				UserVerificationIndex = true,
				UserVerificationMethod = true,
				BiometricAuthenticatorPerformanceBounds = new AuthenticatorBiometricPerfBounds
				{
					FAR = float.MaxValue,
					FRR = float.MaxValue
				}
			};

			var options = fido.RequestNewCredential(fidoUser, keys, selection, AttestationConveyancePreference.None, extensions);
			string uid = Guid.NewGuid().ToString();
			createOptions.Add(uid, (options, isLogin));

			return Json(new JObject
			{
				["uid"] = uid,
				["options"] = JObject.FromObject(options)
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> RegisterWebAuthn(string name, string uid, string data)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			if (string.IsNullOrWhiteSpace(name))
				return BadRequest("No Name given");
			if (string.IsNullOrWhiteSpace(uid))
				return BadRequest("No UID");
			if (string.IsNullOrWhiteSpace(data))
				return BadRequest("No data received");

			var rawResponse = JsonConvert.DeserializeObject<AuthenticatorAttestationRawResponse>(data);
			createOptions.Remove(uid, out var options);

			async Task<bool> Callback(IsCredentialIdUniqueToUserParams args)
			{
				return !(await dbContext.WebAuthnKeys
					.Where(k => k.UserId == SafeInt(args.User.Id))
					.Where(k => k.DescriptorId == ToHex(args.CredentialId))
					.AnyAsync());
			};

			var res = await fido.MakeNewCredentialAsync(rawResponse, options.Options, Callback);
			if (res.Status == "ok")
			{
				var descriptor = new PublicKeyCredentialDescriptor(res.Result.CredentialId);
				dbContext.WebAuthnKeys.Add(new WebAuthnKey
				{
					UserId = authUser.Id,
					Name = name,
					DescriptorId = ToHex(res.Result.CredentialId),
					DescriptorJson = JsonConvert.SerializeObject(descriptor),
					IsLogin = options.IsLogin,
					PublicKey = res.Result.PublicKey,
					RegisteredAt = DateTime.UtcNow
				});
				await dbContext.SaveChangesAsync();
			}

			return Json(res);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult DeleteWebAuthn(string id)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			var key = dbContext.WebAuthnKeys
				.Where(k => k.UserId == authUser.Id)
				.Where(k => k.DescriptorId == id)
				.FirstOrDefault();
			if (key == null)
				return NotFound();

			dbContext.WebAuthnKeys.Remove(key);
			dbContext.SaveChanges();

			return Ok();
		}

		#endregion WebAuthn

		#region Authenticator

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult VerifyTotp(string code)
		{
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			var totp = new Totp(Base32Encoding.ToBytes(authUser.TotpSecret));
			if (totp.VerifyTotp(code, out _))
				return Ok();

			return BadRequest();
		}

		#endregion Authenticator

		#region Enable/Disable MFA

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EnableMfaCode(string code)
		{
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			var totp = new Totp(Base32Encoding.ToBytes(authUser.TotpSecret));
			if (totp.VerifyTotp(code, out _))
			{
				authUser.IsMfaEnabled = true;
				dbContext.SaveChanges();

				await SignIn(HttpContext, authUser, true);
				return Ok();
			}
			return BadRequest();
		}

		public IActionResult GetMfaOptions()
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			var existingCredentials = dbContext.WebAuthnKeys
				.Where(k => !k.IsLogin)
				.Where(k => k.UserId == authUser.Id)
				.Select(k => k.DescriptorJson)
				.AsEnumerable()
				.Select(d => JsonConvert.DeserializeObject<PublicKeyCredentialDescriptor>(d))
				.ToList();
			var extensions = new AuthenticationExtensionsClientInputs
			{
				SimpleTransactionAuthorization = "LoginExperiments WebAuthn",
				GenericTransactionAuthorization = new TxAuthGenericArg
				{
					ContentType = "text/plain",
					Content = Encoding.UTF8.GetBytes("FIDO2")
				},
				UserVerificationIndex = true,
				UserVerificationMethod = true,
				Location = true
			};

			var options = fido.GetAssertionOptions(existingCredentials, UserVerificationRequirement.Discouraged, extensions);
			string uid = Guid.NewGuid().ToString();
			assertOptions.Add(uid, options);

			if (options.RpId == "localhost")
				logger.LogWarning("WebAuthn Relaying Party ID is set to localhost - Assertion may fail! Set correct ASPNETCORE_APPL_URL environment variable to fix this issue.");

			return Json(new JObject
			{
				["uid"] = uid,
				["options"] = JObject.FromObject(options)
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> EnableMfaKey(string uid, string data)
		{
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			if (string.IsNullOrWhiteSpace(uid))
				return BadRequest("No UID");
			if (string.IsNullOrWhiteSpace(data))
				return BadRequest("No data received");

			var rawResponse = JsonConvert.DeserializeObject<AuthenticatorAssertionRawResponse>(data);
			assertOptions.Remove(uid, out var options);

			var credentials = dbContext.WebAuthnKeys
				.Where(k => k.UserId == authUser.Id)
				.Where(k => !k.IsLogin)
				.Where(k => k.DescriptorId == ToHex(rawResponse.Id))
				.FirstOrDefault();
			if (credentials == null)
				return NotFound();

			async Task<bool> Callback(IsUserHandleOwnerOfCredentialIdParams args)
			{
				return await dbContext.WebAuthnKeys
					.Where(k => !k.IsLogin)
					.Where(k => k.UserId == SafeInt(args.UserHandle))
					.Where(k => k.DescriptorId == ToHex(args.CredentialId))
					.AnyAsync();
			};

			uint counter = credentials.Counter;
			var res = await fido.MakeAssertionAsync(rawResponse, options, credentials.PublicKey, counter, Callback);
			credentials.Counter = counter;
			dbContext.SaveChanges();

			if (res.Status == "ok")
			{
				authUser.IsMfaEnabled = true;
				dbContext.SaveChanges();

				await SignIn(HttpContext, authUser, true);
				return Ok();
			}
			return BadRequest();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult DisableMfa()
		{
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			authUser.IsMfaEnabled = false;
			dbContext.SaveChanges();

			return RedirectToAction("Index", "Account");
		}

		#endregion Enable/Disable MFA

		#endregion User profile

		#region Register user

		[AllowAnonymous]
		public IActionResult Register()
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
				return RedirectToAction("Index", "Home");

			ViewData["Title"] = "Register";

			return View(new AccountViewModel());
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public IActionResult Register(AccountViewModel model)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
				return RedirectToAction("Index", "Home");

			ViewData["Title"] = "Register";

			if (string.IsNullOrWhiteSpace(model.Username))
				ModelState.AddModelError(nameof(model.Username), "A username is required");
			if (string.IsNullOrWhiteSpace(model.Password))
				ModelState.AddModelError(nameof(model.Password), "A password is required");
			if (string.IsNullOrWhiteSpace(model.Repeat))
				ModelState.AddModelError(nameof(model.Repeat), "A repetition of the password is required");

			if (!ModelState.IsValid)
				return View(model);

			if (dbContext.Users.Any(u => u.Username == model.Username.Trim().ToLower()))
				ModelState.AddModelError(nameof(model.Username), "The username is already taken");

			if (model.Password.Trim() != model.Repeat.Trim())
				ModelState.AddModelError(nameof(model.Repeat), "The passwords do not match");

			if (!ModelState.IsValid)
				return View(model);

			byte[] bytes = KeyGeneration.GenerateRandomKey(32);
			string secret = Base32Encoding.ToString(bytes).TrimEnd('=');

			dbContext.Users.Add(new User
			{
				Username = model.Username.Trim().ToLower(),
				PasswordHash = SecurityHelper.HashPassword(model.Password.Trim()),
				IsMfaEnabled = false,
				RequireNewPassword = false,
				TotpSecret = secret,
				RecoveryKey = SecurityHelper.GetRandomString(12)
			});
			dbContext.SaveChanges();

			return RedirectToAction("SignIn");
		}

		#endregion Register user

		#region Login

		[AllowAnonymous]
		public IActionResult Login(string returnUrl)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
			{
				if (Url.IsLocalUrl(returnUrl))
					return Redirect(returnUrl);

				return RedirectToAction("Index", "Home");
			}

			ViewData["Title"] = "Login";
			return View(new AccountViewModel());
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(AccountViewModel model, string returnUrl)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser != null)
			{
				if (Url.IsLocalUrl(returnUrl))
					return Redirect(returnUrl);

				return RedirectToAction("Index", "Home");
			}

			ViewData["Title"] = "Login";

			if (string.IsNullOrWhiteSpace(model.Username))
				ModelState.AddModelError(nameof(model.Username), "A username is required");
			if (string.IsNullOrWhiteSpace(model.Password))
				ModelState.AddModelError(nameof(model.Password), "A password is required");

			if (!ModelState.IsValid)
				return View(model);

			var user = dbContext.Users
				.Where(u => u.Username == model.Username.Trim().ToLower())
				.FirstOrDefault();
			if (user == null)
				ModelState.AddModelError(nameof(model.Username), "The username is unknown");

			if (!ModelState.IsValid)
				return View(model);

			if (!SecurityHelper.VerifyPassword(model.Password.Trim(), user.PasswordHash, out bool rehash))
				ModelState.AddModelError(nameof(model.Password), "The password is invalid");

			if (!ModelState.IsValid)
				return View(model);

			if (rehash)
			{
				user.PasswordHash = SecurityHelper.HashPassword(model.Password.Trim());
				dbContext.SaveChanges();
			}

			await SignIn(HttpContext, user);

			if (Url.IsLocalUrl(returnUrl))
				return Redirect(returnUrl);

			return RedirectToAction("Index", "Home");
		}

		[AllowAnonymous]
		public IActionResult GetAssertOptions()
		{
			var existingCredentials = new List<PublicKeyCredentialDescriptor>();
			var extensions = new AuthenticationExtensionsClientInputs
			{
				SimpleTransactionAuthorization = "LoginExperiments WebAuthn",
				GenericTransactionAuthorization = new TxAuthGenericArg
				{
					ContentType = "text/plain",
					Content = Encoding.UTF8.GetBytes("FIDO2")
				},
				UserVerificationIndex = true,
				UserVerificationMethod = true,
				Location = true
			};

			var options = fido.GetAssertionOptions(existingCredentials, UserVerificationRequirement.Discouraged, extensions);
			string uid = Guid.NewGuid().ToString();
			assertOptions.Add(uid, options);

			if (options.RpId == "localhost")
				logger.LogWarning("WebAuthn Relaying Party ID is set to localhost - Login may fail! Set correct ASPNETCORE_APPL_URL environment variable to fix this issue.");

			return Json(new JObject
			{
				["uid"] = uid,
				["options"] = JObject.FromObject(options)
			});
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> AssertLogin(string uid, string data)
		{
			if (string.IsNullOrWhiteSpace(uid))
				return BadRequest("No UID");
			if (string.IsNullOrWhiteSpace(data))
				return BadRequest("No data received");

			var rawResponse = JsonConvert.DeserializeObject<AuthenticatorAssertionRawResponse>(data);
			assertOptions.Remove(uid, out var options);

			var credentials = dbContext.WebAuthnKeys
				.Include(k => k.User)
				.Where(k => k.IsLogin)
				.Where(k => k.DescriptorId == ToHex(rawResponse.Id))
				.FirstOrDefault();
			if (credentials == null)
				return NotFound();

			async Task<bool> Callback(IsUserHandleOwnerOfCredentialIdParams args)
			{
				return await dbContext.WebAuthnKeys
					.Where(k => k.IsLogin)
					.Where(k => k.UserId == SafeInt(args.UserHandle))
					.Where(k => k.DescriptorId == ToHex(args.CredentialId))
					.AnyAsync();
			};

			uint counter = credentials.Counter;
			var res = await fido.MakeAssertionAsync(rawResponse, options, credentials.PublicKey, counter, Callback);
			credentials.Counter = counter;
			dbContext.SaveChanges();

			if (res.Status == "ok")
				await SignIn(HttpContext, credentials.User, true);

			return Json(res);
		}

		public async Task<IActionResult> Logout()
		{
			await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
			return RedirectToAction("Index", "Home");
		}

		internal static async Task SignIn(HttpContext context, User user, bool isMfaSuccess = false)
		{
			await context.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, user.GetPrincipal(isMfaSuccess), new AuthenticationProperties
			{
				AllowRefresh = true,
				IsPersistent = true,
				ExpiresUtc = DateTime.UtcNow.AddDays(1)
			});
		}

		#endregion Login

		#region Change password

		public IActionResult ChangePassword()
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			ViewData["Title"] = "Change Password";
			return View(new AccountViewModel());
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult ChangePassword(AccountViewModel model, string returnUrl)
		{
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			ViewData["Title"] = "Change Password";

			if (string.IsNullOrWhiteSpace(model.Current))
				ModelState.AddModelError(nameof(model.Current), "A current password is required");
			if (string.IsNullOrWhiteSpace(model.Password))
				ModelState.AddModelError(nameof(model.Password), "A new password is required");

			if (!ModelState.IsValid)
				return View(model);

			if (model.Password.Trim() != model.Repeat?.Trim())
				ModelState.AddModelError(nameof(model.Repeat), "The new password has to be repeated");

			if (!SecurityHelper.VerifyPassword(model.Current, authUser.PasswordHash, out _))
				ModelState.AddModelError(nameof(model.Current), "The password is invalid");

			if (!ModelState.IsValid)
				return View(model);

			authUser.PasswordHash = SecurityHelper.HashPassword(model.Password.Trim());
			authUser.RequireNewPassword = false;
			dbContext.SaveChanges();

			if (Url.IsLocalUrl(returnUrl))
				return Redirect(returnUrl);

			return RedirectToAction("Index", "Home");
		}

		#endregion Change password

		#region Helper

		internal static string ToHex(IEnumerable<byte> bytes)
		{
			var sb = new StringBuilder();
			foreach (byte b in bytes)
				sb.Append(b.ToString("x2"));

			return sb.ToString();
		}

		internal static int SafeInt(byte[] bytes)
		{
			if (bytes.Length != sizeof(int))
				throw new ArgumentException();

			byte[] ar = bytes.ToArray(); // duplicate
			if (BitConverter.IsLittleEndian)
				Array.Reverse(ar);

			return BitConverter.ToInt32(ar);
		}

		internal static byte[] SafeInt(int number)
		{
			byte[] bytes = BitConverter.GetBytes(number);
			if (BitConverter.IsLittleEndian)
				Array.Reverse(bytes);

			return bytes;
		}

		#endregion Helper
	}
}
