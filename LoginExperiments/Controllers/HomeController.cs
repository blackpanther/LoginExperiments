﻿using LoginExperiments.Database;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace LoginExperiments.Controllers
{
	public class HomeController : Controller
	{
		private readonly MyContext dbContext;

		public HomeController(MyContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public IActionResult Index()
		{
			ViewData["Title"] = "Overview";

			ViewData["Users"] = dbContext.Users.ToList();

			return View();
		}
	}
}