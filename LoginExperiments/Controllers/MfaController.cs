﻿using Fido2NetLib;
using Fido2NetLib.Objects;
using LoginExperiments.Database;
using LoginExperiments.Extensions;
using LoginExperiments.Models;
using LoginExperiments.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OtpNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginExperiments.Controllers
{
	[Authorize]
	public class MfaController : Controller
	{
		private readonly ILogger logger;
		private readonly MyContext dbContext;
		private readonly IFido2 fido;

		private static readonly Dictionary<string, AssertionOptions> assertOptions = new Dictionary<string, AssertionOptions>();

		public MfaController(ILogger<MfaController> logger, MyContext dbContext, IFido2 fido)
		{
			this.logger = logger;
			this.dbContext = dbContext;
			this.fido = fido;
		}

		public IActionResult Index()
		{
			ViewData["Title"] = "Multi-Factor Authentication";
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			ViewData["Tab"] = "totp";
			return View(new MfaViewModel());
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Totp(MfaViewModel model, string returnUrl)
		{
			ViewData["Title"] = "Multi-Factor Authentication";
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			ViewData["Tab"] = "totp";
			HttpContext.Items[HttpItem.RequestPath] = returnUrl;

			var totp = new Totp(Base32Encoding.ToBytes(authUser.TotpSecret));
			if (totp.VerifyTotp(model.TotpKey, out _))
			{
				await AccountController.SignIn(HttpContext, authUser, true);
				if (Url.IsLocalUrl(HttpContext.GetOriginalRequest()))
					return Redirect(HttpContext.GetOriginalRequest());

				return RedirectToAction("Index", "Home");
			}

			ModelState.AddModelError(nameof(model.TotpKey), "The code is invalid");
			return View("Index", model);
		}

		public IActionResult GetOptions()
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			var existingCredentials = dbContext.WebAuthnKeys
				.Where(k => !k.IsLogin)
				.Where(k => k.UserId == authUser.Id)
				.Select(k => k.DescriptorJson)
				.AsEnumerable()
				.Select(d => JsonConvert.DeserializeObject<PublicKeyCredentialDescriptor>(d))
				.ToList();
			var extensions = new AuthenticationExtensionsClientInputs
			{
				SimpleTransactionAuthorization = "LoginExperiments WebAuthn",
				GenericTransactionAuthorization = new TxAuthGenericArg
				{
					ContentType = "text/plain",
					Content = Encoding.UTF8.GetBytes("FIDO2")
				},
				UserVerificationIndex = true,
				UserVerificationMethod = true,
				Location = true
			};

			var options = fido.GetAssertionOptions(existingCredentials, UserVerificationRequirement.Discouraged, extensions);
			string uid = Guid.NewGuid().ToString();
			assertOptions.Add(uid, options);

			if (options.RpId == "localhost")
				logger.LogWarning("WebAuthn Relaying Party ID is set to localhost - MFA may fail! Set correct ASPNETCORE_APPL_URL environment variable to fix this issue.");

			return Json(new JObject
			{
				["uid"] = uid,
				["options"] = JObject.FromObject(options)
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> MakeAssert(string uid, string data)
		{
			var authUser = HttpContext.GetAuthUser();
			if (authUser == null)
				return Unauthorized();

			if (string.IsNullOrWhiteSpace(uid))
				return BadRequest("No UID");
			if (string.IsNullOrWhiteSpace(data))
				return BadRequest("No data received");

			var rawResponse = JsonConvert.DeserializeObject<AuthenticatorAssertionRawResponse>(data);
			assertOptions.Remove(uid, out var options);

			var credentials = dbContext.WebAuthnKeys
				.Where(k => k.UserId == authUser.Id)
				.Where(k => !k.IsLogin)
				.Where(k => k.DescriptorId == AccountController.ToHex(rawResponse.Id))
				.FirstOrDefault();
			if (credentials == null)
				return NotFound();

			async Task<bool> Callback(IsUserHandleOwnerOfCredentialIdParams args)
			{
				return await dbContext.WebAuthnKeys
					.Where(k => !k.IsLogin)
					.Where(k => k.UserId == AccountController.SafeInt(args.UserHandle))
					.Where(k => k.DescriptorId == AccountController.ToHex(args.CredentialId))
					.AnyAsync();
			};

			var counter = credentials.Counter;
			var res = await fido.MakeAssertionAsync(rawResponse, options, credentials.PublicKey, counter, Callback);
			credentials.Counter = counter;
			dbContext.SaveChanges();

			if (res.Status == "ok")
			{
				await AccountController.SignIn(HttpContext, authUser, true);
				return Ok();
			}
			return BadRequest();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Recovery(MfaViewModel model, string returnUrl)
		{
			ViewData["Title"] = "Multi-Factor Authentication";
			var authUser = HttpContext.GetAuthUser(dbContext);
			if (authUser == null)
				return Unauthorized();

			ViewData["Tab"] = "recovery";
			HttpContext.Items[HttpItem.RequestPath] = returnUrl;

			if (authUser.RecoveryKey == model.RecoveryKey)
			{
				await AccountController.SignIn(HttpContext, authUser, true);
				if (Url.IsLocalUrl(HttpContext.GetOriginalRequest()))
					return Redirect(HttpContext.GetOriginalRequest());

				return RedirectToAction("Index", "Home");
			}

			ModelState.AddModelError(nameof(model.RecoveryKey), "The recovery key is invalid");
			return View("Index", model);
		}
	}
}