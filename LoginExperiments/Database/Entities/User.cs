﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LoginExperiments.Database.Entities
{
	public class User
	{
		#region Database mapped

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		public string Username { get; set; }

		public string Firstname { get; set; }

		public string Lastname { get; set; }

		public string PasswordHash { get; set; }

		public bool RequireNewPassword { get; set; }

		public string TotpSecret { get; set; }

		public bool IsAdmin { get; set; }

		public bool IsMfaEnabled { get; set; }

		public string RecoveryKey { get; set; }

		public virtual List<WebAuthnKey> WebAuthnKeys { get; set; }

		#endregion Database mapped

		#region Not mapped

		[NotMapped]
		public string CurrentPassword { get; set; }

		[NotMapped]
		public string NewPassword { get; set; }

		[NotMapped]
		public string PasswordRepeat { get; set; }

		[NotMapped]
		public bool IsMfaSuccess { get; set; }

		#endregion Not mapped

		public string GetName()
		{
			string name = $"{Firstname} {Lastname}";
			if (string.IsNullOrWhiteSpace(name))
				return Username;

			return name.Trim();
		}
	}
}