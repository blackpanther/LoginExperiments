﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LoginExperiments.Database.Entities
{
	public class WebAuthnKey
	{
		[Key]
		public string DescriptorId { get; set; }

		public string DescriptorJson { get; set; }

		public byte[] PublicKey { get; set; }

		public uint Counter { get; set; }

		[ForeignKey(nameof(User))]
		public int UserId { get; set; }

		public virtual User User { get; set; }

		public bool IsLogin { get; set; }

		public string Name { get; set; }

		public DateTime RegisteredAt { get; set; }
	}
}