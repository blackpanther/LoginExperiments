﻿using LoginExperiments.Database.Entities;
using Microsoft.EntityFrameworkCore;

namespace LoginExperiments.Database
{
	public class MyContext : DbContext
	{
		public MyContext(DbContextOptions<MyContext> options)
			: base(options)
		{ }

		public DbSet<User> Users { get; protected set; }

		public DbSet<WebAuthnKey> WebAuthnKeys { get; protected set; }
	}
}