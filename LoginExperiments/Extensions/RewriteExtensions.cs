﻿using LoginExperiments.Database;
using LoginExperiments.Utils;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace LoginExperiments.Extensions
{
	public static class RewriteExtensions
	{
		private static readonly string[] staticPathBegins = new[]
		{
			"/css",
			"/js",
			"/lib"
		};

		public static RewriteOptions AddMfa(this RewriteOptions options, IServiceScopeFactory serviceScopeFactory)
		{
			var allowedPaths = staticPathBegins.Concat(new[]
			{
				"/account/sign",    // signin/-out
				"/mfa"
			});

			options.Add(rewriteContext =>
			{
				// These paths are allowed and not redirected
				foreach (string pathBegin in allowedPaths)
				{
					if (rewriteContext.HttpContext.Request.Path.ToString().StartsWith(pathBegin))
						return;
				}

				using var scope = serviceScopeFactory.CreateScope();
				var dbContext = scope.ServiceProvider.GetService<MyContext>();

				var authUser = rewriteContext.HttpContext.GetAuthUser(dbContext);

				// User is not authenticated
				if (authUser == null)
					return;

				// Multi-factor authentication is not enabled
				if (!authUser.IsMfaEnabled)
					return;

				// Multi-factor authentication was already successful
				if (authUser.IsMfaSuccess)
					return;

				rewriteContext.Result = RuleResult.SkipRemainingRules;
				rewriteContext.HttpContext.Items[HttpItem.RequestPath] = rewriteContext.HttpContext.Request.Path.ToString();
				rewriteContext.HttpContext.Request.Path = "/mfa";
			});

			return options;
		}

		public static RewriteOptions AddUserChecks(this RewriteOptions options, IServiceScopeFactory serviceScopeFactory)
		{
			var allowedPaths = staticPathBegins.Concat(new[]
			{
				"/account/changepassword",
				"/mfa"
			});

			options.Add(rewriteContext =>
			{
				// These paths are allowed and not redirected
				foreach (string pathBegin in allowedPaths)
				{
					if (rewriteContext.HttpContext.Request.Path.ToString().StartsWith(pathBegin))
						return;
				}

				using var scope = serviceScopeFactory.CreateScope();
				var dbContext = scope.ServiceProvider.GetService<MyContext>();

				var authUser = rewriteContext.HttpContext.GetAuthUser(dbContext);

				// User is not authenticated
				if (authUser == null)
					return;

				// User is forced to change the password
				if (authUser.RequireNewPassword)
				{
					rewriteContext.Result = RuleResult.SkipRemainingRules;
					rewriteContext.HttpContext.Items[HttpItem.RequestPath] = rewriteContext.HttpContext.Request.Path.ToString();
					rewriteContext.HttpContext.Request.Path = "/account/changepassword";
					return;
				}
			});

			return options;
		}
	}
}