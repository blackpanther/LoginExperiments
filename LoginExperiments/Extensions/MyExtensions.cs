﻿using LoginExperiments.Database;
using LoginExperiments.Database.Entities;
using LoginExperiments.Utils;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace LoginExperiments.Extensions
{
	public static class MyExtensions
	{
		public static ClaimsPrincipal GetPrincipal(this User user, bool isMfaSuccess = false)
		{
			var claims = new List<Claim>
			{
				new Claim(LoginClaim.UserId, user.Id.ToString()),
				new Claim(LoginClaim.Username, user.Username),
				new Claim(LoginClaim.Firstname, user.Firstname ?? ""),
				new Claim(LoginClaim.Lastname, user.Lastname ?? ""),
				new Claim(LoginClaim.IsAdmin, user.IsAdmin.ToString()),
				new Claim(LoginClaim.MfaEnabled, user.IsMfaEnabled.ToString()),
				new Claim(LoginClaim.MfaSuccess, isMfaSuccess.ToString())
			};
			var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
			return new ClaimsPrincipal(identity);
		}

		public static User GetUser(this ClaimsPrincipal principal, MyContext dbContext = null)
		{
			if (int.TryParse(principal.FindFirstValue(LoginClaim.UserId), out int userId))
			{
				User user = null;

				if (dbContext == null)
				{
					user = new User
					{
						Id = userId,
						Username = principal.FindFirstValue(LoginClaim.Username),
						Firstname = principal.FindFirstValue(LoginClaim.Firstname),
						Lastname = principal.FindFirstValue(LoginClaim.Lastname),
						IsAdmin = bool.Parse(principal.FindFirstValue(LoginClaim.IsAdmin)),
						IsMfaEnabled = bool.Parse(principal.FindFirstValue(LoginClaim.MfaEnabled))
					};
				}
				else
				{
					user = dbContext.Users
						.Where(u => u.Id == userId)
						.FirstOrDefault();
				}

				if (user != null)
					user.IsMfaSuccess = bool.Parse(principal.FindFirstValue(LoginClaim.MfaSuccess));

				return user;
			}
			return null;
		}

		public static User GetAuthUser(this HttpContext httpContext, MyContext dbContext = null)
		{
			return httpContext.User?.GetUser(dbContext);
		}

		public static string GetOriginalRequest(this HttpContext httpContext)
		{
			string path = httpContext.Items[HttpItem.RequestPath]?.ToString();
			if (string.IsNullOrWhiteSpace(path))
				path = httpContext.Request.Query[HttpItem.ReturnUrl].ToString();

			return path;
		}

		public static (string Name, string Value) GetAntiforgeryToken(this HttpContext httpContext)
		{
			var af = httpContext.RequestServices.GetService<IAntiforgery>();
			var set = af?.GetAndStoreTokens(httpContext);

			return (Name: set?.FormFieldName, Value: set?.RequestToken);
		}
	}
}