﻿using LoginExperiments.Utils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Net;

namespace LoginExperiments.Extensions
{
	public static class AspNetCoreExtensions
	{
		public static void UseProxyHosting(this IApplicationBuilder app)
		{
			string path = Environment.GetEnvironmentVariable("ASPNETCORE_APPL_PATH");
			if (!string.IsNullOrWhiteSpace(path))
				app.UsePathBase(new PathString(path));

			var options = new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.All };
			options.KnownNetworks.Clear();

			options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("10.0.0.0"), 8));
			options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("172.16.0.0"), 12));
			options.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("192.168.0.0"), 16));

			app.UseForwardedHeaders(options);
		}

		public static IServiceCollection AddSingletonHostedService<TService>(this IServiceCollection services)
			where TService : class, IHostedService
		{
			services.AddSingleton<TService>();
			services.AddSingleton<IHostedService, BackgroundServiceStarter<TService>>();
			return services;
		}
	}
}