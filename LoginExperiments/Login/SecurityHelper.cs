﻿using LoginExperiments.Database;
using LoginExperiments.Database.Entities;
using LoginExperiments.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;

namespace LoginExperiments.Login
{
	public static class SecurityHelper
	{
		public static string HashPassword(string plain)
		{
			if (plain == null)
				return null;

			var ph = new PasswordHasher<object>();
			return ph.HashPassword(null, plain.Trim());
		}

		public static bool VerifyPassword(string plain, string hash, out bool needRehash)
		{
			needRehash = false;

			if (plain == null || string.IsNullOrWhiteSpace(hash))
				return false;

			var ph = new PasswordHasher<object>();
			var result = ph.VerifyHashedPassword(null, hash.Trim(), plain.Trim());
			switch (result)
			{
				case PasswordVerificationResult.Success:
					return true;

				case PasswordVerificationResult.SuccessRehashNeeded:
					needRehash = true;
					return true;

				case PasswordVerificationResult.Failed:
				default:
					return false;
			}
		}

		public static string GetRandomString(int length, string pool = null)
		{
			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length));

			if (string.IsNullOrWhiteSpace(pool))
			{
				pool  = "abcdefghijklmnopqrstuvwxyz";
				pool += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				pool += "0123456789";
			}

			string str = "";
			var rnd = new Random();
			while (length-- > 0)
			{
				int rand = rnd.Next(0, pool.Length);
				str += pool[rand];
			}

			return str;
		}
	}
}