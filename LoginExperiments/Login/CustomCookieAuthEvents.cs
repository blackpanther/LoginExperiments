﻿using LoginExperiments.Database;
using LoginExperiments.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;

namespace LoginExperiments.Login
{
	public class CustomCookieAuthEvents : CookieAuthenticationEvents
	{
		private readonly MyContext dbContext;

		public CustomCookieAuthEvents(MyContext dbContext)
		{
			this.dbContext = dbContext;
		}

		public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
		{
			var httpUser = context.Principal.GetUser();
			if (httpUser == null)
			{
				context.RejectPrincipal();
				await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
				return;
			}

			var dbUser = context.Principal.GetUser(dbContext);
			if (dbUser == null || httpUser.Username != dbUser.Username)
			{
				context.RejectPrincipal();
				await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
				return;
			}

			if (httpUser.Firstname != dbUser.Firstname ||
				httpUser.Lastname != dbUser.Lastname ||
				httpUser.IsAdmin != dbUser.IsAdmin ||
				httpUser.IsMfaEnabled != dbUser.IsMfaEnabled)
			{
				context.ReplacePrincipal(dbUser.GetPrincipal(httpUser.IsMfaSuccess));
				context.ShouldRenew = true;
			}
		}
	}
}