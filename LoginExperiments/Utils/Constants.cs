﻿namespace LoginExperiments.Utils
{
	public class LoginClaim
	{
		public const string UserId = "UserId";
		public const string Username = "Username";
		public const string Firstname = "Firstname";
		public const string Lastname = "Lastname";
		public const string IsAdmin = "IsAdmin";

		public const string MfaEnabled = "IsMfaEnabled";
		public const string MfaSuccess = "WasMfaSuccessful";
	}

	public class HttpItem
	{
		public const string RequestPath = "RequestPath";
		public const string OriginalPath = "OriginalPath";
		public const string ReturnUrl = "returnUrl";
	}
}