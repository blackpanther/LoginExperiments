﻿using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace LoginExperiments.Utils
{
	public class BackgroundServiceStarter<TService> : IHostedService
		where TService : class, IHostedService
	{
		private readonly TService service;

		public BackgroundServiceStarter(TService backgroundService)
		{
			service = backgroundService;
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			return service.StartAsync(cancellationToken);
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			return service.StopAsync(cancellationToken);
		}
	}
}