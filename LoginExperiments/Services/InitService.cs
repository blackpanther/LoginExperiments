﻿using LoginExperiments.Database;
using LoginExperiments.Database.Entities;
using LoginExperiments.Login;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OtpNet;
using System.Threading;
using System.Threading.Tasks;

namespace LoginExperiments.Services
{
	public class InitService : IHostedService
	{
		private readonly IServiceScopeFactory serviceScopeFactory;

		public InitService(IServiceScopeFactory serviceScopeFactory)
		{
			this.serviceScopeFactory = serviceScopeFactory;
		}

		public async Task StartAsync(CancellationToken cancellationToken)
		{
			using var scope = serviceScopeFactory.CreateScope();
			var dbContext = scope.ServiceProvider.GetRequiredService<MyContext>();
			var config = scope.ServiceProvider.GetRequiredService<IConfiguration>();

			if (!await dbContext.Users.AnyAsync(cancellationToken))
			{
				byte[] bytes = KeyGeneration.GenerateRandomKey(32);
				string secret = Base32Encoding.ToString(bytes).TrimEnd('=');

				dbContext.Users.Add(new User
				{
					Username = config.GetValue("admin_user", "admin"),
					PasswordHash = SecurityHelper.HashPassword(config.GetValue("admin_password", "admin")),
					IsAdmin = true,
					IsMfaEnabled = false,
					RequireNewPassword = false,
					TotpSecret = secret,
					RecoveryKey = SecurityHelper.GetRandomString(12)
				});
				await dbContext.SaveChangesAsync(cancellationToken);
			}
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}
	}
}