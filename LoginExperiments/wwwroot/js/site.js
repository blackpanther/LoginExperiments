﻿$(function () {
	$('.toggle-password').click(event => {
		event.preventDefault();
		let button = $(event.currentTarget);
		let input = button.closest('.input-group').find('input').first();

		if (button.is(':disabled') || input.is(':disabled'))
			return;

		if (input.attr('type') === 'password') {
			input.attr('type', 'text');
			button.text('Hide');
		}
		else {
			input.attr('type', 'password');
			button.text('Show');
		}
	});
});

// https://github.com/mozilla/serviceworker-cookbook/blob/master/tools.js
var urlBase64ToUint8Array = base64String => {
	let padding = '='.repeat((4 - base64String.length % 4) % 4);
	let base64 = (base64String + padding)
		.replace(/-/g, '+')
		.replace(/_/g, '/');

	let raw = atob(base64);
	let array = new Uint8Array(raw.length);

	for (let i = 0; i < raw.length; ++i) {
		array[i] = raw.charCodeAt(i);
	}
	return array;
}

// https://github.com/abergs/fido2-net-lib/blob/master/Demo/wwwroot/js/helpers.js
var coerceToArrayBuffer = thing => {
	if (typeof thing === 'string') {
		thing = urlBase64ToUint8Array(thing);
	}

	if (Array.isArray(thing))
		thing = new Uint8Array(thing);

	if (thing instanceof Uint8Array)
		thing = thing.buffer;

	if (!(thing instanceof ArrayBuffer))
		throw new TypeError("Could not coerce to ArrayBuffer");

	return thing;
}

var base64Encode = arrayBuffer => {
	let array = new Uint8Array(arrayBuffer);
	let str = String.fromCharCode.apply(null, array);
	return btoa(str);
}