using System;

namespace LoginExperiments.Models
{
	public class ErrorViewModel
	{
		public string Title { get; set; }

		public string Description { get; set; }

		public int ErrorCode { get; set; }
	}
}
