﻿namespace LoginExperiments.Models
{
	public class AccountViewModel
	{
		public string Username { get; set; }

		public string Current { get; set; }

		public string Password { get; set; }

		public string Repeat { get; set; }
	}
}