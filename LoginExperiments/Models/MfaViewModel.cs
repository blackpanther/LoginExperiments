﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginExperiments.Models
{
	public class MfaViewModel
	{
		public string TotpKey { get; set; }

		public string RecoveryKey { get; set; }
	}
}
