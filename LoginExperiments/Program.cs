using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace LoginExperiments
{
	public class Program
	{
		public static int Main(string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += (s, a) =>
			{
				string term = a.IsTerminating ? " (terminating)" : "";
				if (a.ExceptionObject is Exception ex)
				{
					Console.Error.WriteLine($"Unhandled exception in AppDomain{term}: {ex.InnerException?.Message ?? ex.Message}");
				}
				else
				{
					Console.Error.WriteLine($"Unhandled exception in AppDomain{term}: {a.ExceptionObject}");
				}
			};

#if DEBUG
			Console.Write("Waiting for debugger to attach...");
			SpinWait.SpinUntil(() => Debugger.IsAttached);
			Console.WriteLine(" done");
			Console.WriteLine();
#endif
			try
			{
				Console.WriteLine("LoginExperiments startig...");

				var host = CreateHostBuilder(args).Build();
				try
				{
					host.Start();
					Console.WriteLine("LoginExperiments started.");

					host.WaitForShutdown();
					Thread.Sleep(500);   // Ensure messages are written in correct order
					Console.WriteLine("LoginExperiments shut down");
					Thread.Sleep(500);   // Ensure message is written
				}
				finally
				{
					host.Dispose();
				}
				return 0;
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine($"Unhandled exception Main: {ex.InnerException?.Message ?? ex.Message}");
				Console.WriteLine("LoginExperiments shut down uncleanly");
				Thread.Sleep(500);
				return 1;
			}
		}

		private static IHostBuilder CreateHostBuilder(string[] args)
		{
			string appDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string dir = Directory.GetCurrentDirectory();
			string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

			return Host.CreateDefaultBuilder(args)
				.ConfigureServices(services =>
				{
					services.AddOptions<HostOptions>().Configure(options => options.ShutdownTimeout = TimeSpan.FromSeconds(60));
				})
				.ConfigureWebHostDefaults(webHostBuilder =>
				{
					webHostBuilder.UseKestrel();
					webHostBuilder.UseContentRoot(dir);
					webHostBuilder.ConfigureAppConfiguration((_, configuration) =>
					{
						configuration.SetBasePath(dir);
						configuration.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
						configuration.AddEnvironmentVariables();
						if (args?.Any() == true)
							configuration.AddCommandLine(args);
					});
					webHostBuilder.UseDefaultServiceProvider((_, options) =>
					{
						options.ValidateScopes = env == "Development";
					});
					webHostBuilder.UseStartup<Startup>();
				});
		}
	}
}